ActiveAdmin.register MenuItem do

  permit_params :title, :link

  config.filters = false

  config.sort_order = 'position_asc'
  config.paginate = false

  sortable tree: true

  index as: :sortable do
    label do |item|
      if item.link.blank?
        item.title
      else
        link_to item.title, URI(item.link.to_s).host ? URI(item.link).to_s : URI.join(root_url, item.link).to_s
      end
    end
    actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :link
    end
    f.actions
  end

  show do |item|
    attributes_table do
      row :title
      row :link do
        unless item.link.blank?
          link_to item.link, URI(item.link.to_s).host ? URI(item.link).to_s : URI.join(root_url, item.link).to_s
        end
      end
      row :created_at
      row :updated_at
    end
  end

end
