ActiveAdmin.register Event do

  actions :all


  permit_params :title, :description, :date, :end_date, :location, :hoster_name, :hoster_email, :code, :user_id, :date_date, :date_time_hour, :date_time_minute, :end_date_date, :end_date_time_hour, :end_date_time_minute


  filter :user
  filter :date

  member_action :generate_invoice, method: :put do
    event = Event.find(params[:id])

    event.generate_commission_invoice
    event.generate_coupon

    flash[:notice] = "Commission invoice generated"
    redirect_to admin_events_path
  end

  index do
    column :title
    column :date, sortable: :date do |event|
      event.date.strftime('%d/%m/%Y')
    end
    column :location
    column :hoster, sortable: :hoster_name do |event|
      mail_to event.hoster_email, event.hoster_name
    end
    column :user
    column :commission_invoice do |event|
      if event.ended?
        if event.commission_invoice
          link_to "Show invoice", admin_commission_invoice_path(id: event.commission_invoice)
        else
          link_to "Generate commission invoice", {action: :generate_invoice, id: event}, method: :put
        end
      else
        "Event in progress..."
      end
    end
    actions do |event|

    end
  end

  form do |f|
    f.inputs do
      f.input :user
      f.input :title
      f.input :description
      f.input :date, as: :just_datetime_picker
      f.input :end_date, as: :just_datetime_picker
      f.input :location
      f.input :hoster_name
      f.input :hoster_email
    end
    f.actions
  end

  show do |event|
    attributes_table do
      row :title
      row :user
      row :description
      row :location
      row :hoster do
        mail_to event.hoster_email, event.hoster_name
      end
      row :code
      row :date do
        event.date.strftime('%d/%m/%Y')
      end
      row :end_date do
        event.end_date.strftime('%d/%m/%Y')
      end
    end
  end

end
