ActiveAdmin.register Slide do

  permit_params :title, :description,
    :image_attributes => [:id, :file, :file_cache, :_destroy]

  config.filters = false

  config.sort_order = 'position_asc'
  config.paginate = false

  sortable

  index as: :sortable do
    # sortable_handle_column

    label :title
    # label do |s|
    #   image_tag(s.image.file.url(:tiny_thumb))
    # end
    actions
  end

  form html: {multipart: true} do |f|
    f.inputs do
      f.input :title
      # f.input :description, as: :ckeditor
      # f.inputs name: "Image", for: :image do |fi|
      #   fi.input :file
      # end
    end
    f.inputs for: :image do |fi|
      fi.input :file, label: "Image", hint: image_tag(f.object.image.file.url(:thumb))
      fi.input :file_cache, as: :hidden
    end


    f.actions
  end

  show do |slide|
    attributes_table do
      row :title
      # row :description do
      #   slide.description.html_safe
      # end
      row :image do
        link_to(image_tag(slide.image.file.url(:tiny_thumb)), slide.image.file.url).html_safe if slide.image.file.file
      end
      row :created_at
      row :updated_at
    end
  end

end
