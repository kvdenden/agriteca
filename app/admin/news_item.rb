ActiveAdmin.register NewsItem do

  permit_params :title, :body,
    :image_attributes => [:id, :file, :file_cache, :_destroy]
  
  config.filters = false

  index do
    column :title
    actions
  end

  form html: {multipart: true} do |f|
    f.inputs do
      f.input :title
      f.input :body, as: :ckeditor
    end
    f.inputs for: :image do |fi|
      fi.input :file, label: "Image", hint: image_tag(f.object.image.file.url(:thumb))
    end
    f.actions
  end

  show do |news_item|
    attributes_table do
      row :title
      row :body do
        news_item.body.html_safe
      end
      row :image do
        link_to(image_tag(news_item.image.file.url(:tiny_thumb)), news_item.image.file.url).html_safe
      end
      row :created_at
      row :updated_at
    end
  end

end
