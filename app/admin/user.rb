ActiveAdmin.register User, as: "Consultant" do

  permit_params :email, :password, :approved,
    :profile_attributes => [:id, :first_name, :last_name, :address1, :address2, :city, :postal_code, :country, :phone_number, :parent_id, :gets_commission]

  scope :all
  scope :approved
  scope :unapproved

  filter :email

  index do
    column :name do |user|
      user.profile.first_name.to_s + " " + user.profile.last_name.to_s
    end
    column :email, sortable: false
    column :approved
    actions
  end

  form do |f|
    f.inputs for: :profile do |fp|
      fp.input :first_name
      fp.input :last_name
      fp.input :address1
      fp.input :address2
      fp.input :city
      fp.input :postal_code
      fp.input :country
      fp.input :phone_number
      fp.input :parent, label: t('parent')
      fp.input :gets_commission
    end
    f.inputs do
      f.input :email
      f.input :password
      f.input :approved
    end
    f.actions
  end

  show do |user|
    attributes_table do
      row :name do
        user.profile.name
      end
      row :address do
        user.profile.full_address
      end
      row :phone_number do
        user.profile.phone_number
      end
      row :email do
        mail_to user.email, user.email
      end
      row :parent do
        user.parent
      end
      row :gets_commission do
        status_tag user.profile.gets_commission? ? "yes" : "no"
      end

      row :approved do
        status_tag user.approved? ? "yes" : "no"
      end
      # row :
      # row :email
      # row :full_address
      # row :items do
      #   items = order.line_items.map do |item|
      #     variations = item.variations.map { |v| "#{v.option}: #{v.variation}"}.join(", ")
      #     content_tag :li, "#{content_tag :strong, item.quantity} x #{item.product.title} <br /> #{content_tag :small, variations}".html_safe
      #   end
      #   content_tag :ul, items.join("").html_safe
      # end
      # row :subtotal do
      #   number_to_currency order.subtotal,  unit: "€"
      # end
      # row :shipping_cost do
      #   number_to_currency order.shipping_cost,  unit: "€"
      # end
      # row :total do
      #   number_to_currency order.total,  unit: "€"
      # end
      # row :paid do
      #   status_tag order.paid? ? "yes" : "no"
      # end
      # row :weight do
      #   order.weight.to_s + 'gr'
      # end
      # row :shipping_type do
      #   order.shipping_type(:name)
      # end
      # row :from_country do
      #   order.from_country(:name)
      # end

    end
  end

end

  # create_table "orders", force: true do |t|
  #   t.string   "name"
  #   t.string   "address1"
  #   t.string   "address2"
  #   t.string   "city"
  #   t.string   "postal_code"
  #   t.integer  "country"
  #   t.string   "phone_number"
  #   t.decimal  "shipping_cost", precision: 10, scale: 2
  #   t.datetime "created_at"
  #   t.datetime "updated_at"
  #   t.string   "email"
  #   t.boolean  "paid",                                   default: false
  #   t.integer  "shipping_type"
  #   t.integer  "from_country"
  # end
