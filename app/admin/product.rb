ActiveAdmin.register Product do
  
  permit_params :title, :description, :code, :weight, :price, :category_id, :tag_list, :published, :primary, :ships_from_belgium, :adult_only, :shipping_type,
    :option_ids => [],
    :images_attributes => [:id, :file, :file_cache, :_destroy]

  controller do
    defaults finder: :find_by_slug
  end

  scope :all
  scope :published
  scope :unpublished

  # filter :published
  filter :category
  filter :primary
  filter :title
  # filter :price
  # filter :tags

  index do
    column :title
    column :code
    column :category
    column :weight, sortable: :weight do |product|
      product.weight.to_s + 'gr'
    end
    column :price, sortable: :price do |product|
      number_to_currency product.price,  unit: "€"
    end
    column :published
    # column :primary
    # column :ships_from_be
    # column :adult_only
    # column "Tags", :tag_list, sortable: false
    actions
  end
  
  form html: {multipart: true} do |f|
    f.inputs do
      f.input :title
      f.input :code
      f.input :category
      f.input :description, as: :ckeditor
      f.input :weight
      f.input :price
      f.input :options, as: :select
      f.input :published
      f.input :primary
      f.input :shipping_type
      f.input :ships_from_belgium
      f.input :adult_only
      f.has_many :images, allow_destroy: true, heading: "Images" do |fi|
        fi.input :file, hint: image_tag(fi.object.file.url(:thumb))
        fi.input :file_cache, as: :hidden
      end
      # f.input :tag_list, label: "Tags"
    end
    f.actions
  end

  show do |product|
    attributes_table do
      
      row :title
      row :code
      row :published do
        status_tag product.published? ? "yes" : "no"
      end
      row :description do
        product.description.html_safe
      end
      row :category
      row :price do
        number_to_currency product.price,  unit: "€"
      end
      row :weight do
        product.weight.to_s + 'gr'
      end
      row :shipping_type do
        product.shipping_type(:name)
      end
      row :primary do
        status_tag product.primary? ? "yes" : "no"
      end
      row :ships_from_belgium do
        status_tag product.ships_from_belgium? ? "yes" : "no"
      end
      row :adult_only do
        status_tag product.adult_only? ? "yes" : "no"
      end
      row :images do
        product.images.map { |image| link_to image_tag(image.file.url(:tiny_thumb)), image.file.url }.join(' ').html_safe
      end
      # row :tags do
      #   product.tag_list
      # end
      row :path do
        link_to product_path(product), product_path(product)
      end
      row :created_at
      row :updated_at
    end
  end

end
