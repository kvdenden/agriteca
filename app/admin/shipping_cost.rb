ActiveAdmin.register ShippingCost do

  permit_params :from_country, :to_country, :shipping_type, :max_weight, :cost

  filter :from_country, as: :select, collection: Country.to_select
  filter :to_country, as: :select, collection: Country.to_select
  filter :shipping_type, as: :select, collection: ShippingType.to_select

  index do
    column :max_weight, sortable: :max_weight do |cost|
      cost.max_weight.to_s + 'gr'
    end
    column :from_country do |cost|
      cost.from_country(:name)
    end
    column :to_country do |cost|
      cost.to_country(:name)
    end
    column :shipping_type do |cost|
      cost.shipping_type(:name)
    end
    column :cost, sortable: :cost do |cost|
      number_to_currency cost.cost,  unit: "€"
    end
    actions
  end

  show do |cost|
    attributes_table do
      row :from_country do
        cost.from_country(:name)
      end
      row :to_country do
        cost.to_country(:name)
      end
      row :shipping_type do
        cost.shipping_type(:name)
      end
      row :max_weight do
        cost.max_weight.to_s + 'gr'
      end
      row :cost do
        number_to_currency cost.cost,  unit: "€"
      end
    end
  end
  
end