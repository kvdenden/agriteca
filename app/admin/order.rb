ActiveAdmin.register Order do

  actions :all, :except => [:edit]

  permit_params :name, :address1, :address2, :city, :postal_code, :country, :phone_number, :email, :status, :paid, :code, :vat_number

  scope :all
  scope :paid
  scope :unpaid

  filter :name
  filter :email
  filter :country, as: :select, collection: Country.to_select

  index do
    column :paid
    column :date, sortable: :created_at do |order|
      order.created_at.strftime('%d/%m/%Y')
    end
    column :code do |order|
      order.event ? order.event.code : ''
    end
    column :first_name
    column :name
    column t('billing_address'), :full_address, sortable: false
    column t('delivery_address'), :full_delivery_address, sortable: false
    column :email
    column :vat_number
    # column :phone_number
    # column :weight, sortable: :weight do |order|
    #   order.weight.to_s + 'gr'
    # end
    column :total_without_vat do |order|
      number_to_currency order.total_without_vat,  unit: "€"
    end
    column :vat do |order|
      number_to_currency order.vat,  unit: "€"
    end
    column :shipping_cost do |order|
      number_to_currency order.shipping_cost,  unit: "€"
    end
    column :total_with_vat do |order|
      number_to_currency order.total_with_vat,  unit: "€"
    end
    column :invoice do |order|
      link_to order.invoice.code, "/invoices/#{order.invoice.code}.pdf" if order.invoice
    end
    # column :consultant do |order|
    #   order.event && order.event.user ? order.event.user.name : ''
    # end
    actions
  end

  form do |f|
    f.inputs do
      f.input :first_name
      f.input :name
      f.input :phone_number
      f.input :email
      f.input :vat_number
      f.input :address1
      f.input :address2
      f.input :postal_code
      f.input :city
      f.input :country
      f.input :code
      f.input :status
      f.input :paid
    end
    f.actions
  end

  show do |order|
    attributes_table do
      row :first_name
      row :name
      row :phone_number
      row :email
      row :vat_number
      row :full_address
      row :full_delivery_address
      row :event do
        order.event ? link_to(order.event.title, order.event) : ''
      end
      row :items do
        items = order.line_items.map do |item|
          variations = item.variations.map { |v| "#{v.option}: #{v.variation}"}.join(", ")
          content_tag :li, "#{content_tag :strong, item.quantity} x #{item.product.title} <br /> #{content_tag :small, variations}".html_safe
        end
        content_tag :ul, items.join("").html_safe
      end
      row :subtotal do
        number_to_currency order.subtotal,  unit: "€"
      end
      row :shipping_cost do
        number_to_currency order.shipping_cost,  unit: "€"
      end
      row :vat do
        number_to_currency order.vat,  unit: "€"
      end
      row :total do
        number_to_currency order.total_with_vat,  unit: "€"
      end
      row :status
      row :paid do
        status_tag order.paid? ? "yes" : "no"
      end
      row :weight do
        order.weight.to_s + 'gr'
      end
      row :shipping_type do
        order.shipping_type(:name)
      end
      row :from_country do
        order.from_country(:name)
      end
      row :invoice do
        link_to order.invoice.code, "/invoices/#{order.invoice.code}.pdf" if order.invoice
      end

    end
  end

end
