ActiveAdmin.register Attribute do

  permit_params :title,
    :variations_attributes => [:id, :title, :_destroy]

  menu parent: "Products"
  
  index do
    column :title
    column :variations do |a|
      a.variations.map { |v| v.title }.join(", ")
    end
    actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.has_many :variations, allow_destroy: true, heading: "Variations" do |fv|
        fv.input :title
      end
    end
    f.actions
  end

  show do |attribute|
    attributes_table do
      row :title
      row :variations do
        variations = attribute.variations
        variations.empty? ? '' : content_tag(:ul, variations.map { |v| content_tag :li, v.title }.join('').html_safe)
      end
      row :created_at
      row :updated_at
    end
  end
  
end
