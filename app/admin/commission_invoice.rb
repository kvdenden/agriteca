ActiveAdmin.register CommissionInvoice do

  actions :index, :show

  filter :invoice_type, as: :select, collection: ['event', 'monthly']
  filter :year

  index do
    column :invoice_type
    column :code
    column :user
    column :event
    column :total do |invoice|
      number_to_currency invoice.total,  unit: "€"
    end
    actions
  end

  show do |invoice|
    attributes_table do
      row :invoice_type
      row :code
      row :user
      row :event
      row :total do
        number_to_currency invoice.total,  unit: "€"
      end
      row :created_at do
        invoice.created_at.strftime('%d/%m/%Y')
      end
      row :download do
        link_to invoice.code, "/invoices/commission/#{invoice.code}.pdf"
      end
    end
  end


end

  # create_table "commission_invoices", force: true do |t|
  #   t.string   "code"
  #   t.integer  "user_id"
  #   t.integer  "month"
  #   t.integer  "year"
  #   t.string   "invoice_type"
  #   t.datetime "created_at"
  #   t.datetime "updated_at"
  #   t.integer  "event_id"
  #   t.decimal  "total",        precision: 10, scale: 2
  # end

#   ActiveAdmin.register Event do

#   actions :all, :except => [:edit, :destroy]


#   permit_params :title, :description, :date, :end_date, :location, :hoster_name, :hoster_email, :code, :user_id


#   filter :user
#   filter :date

#   index do
#     column :title
#     column :date, sortable: :date do |event|
#       event.date.strftime('%d/%m/%Y')
#     end
#     column :location
#     column :hoster, sortable: :hoster_name do |event|
#       mail_to event.hoster_email, event.hoster_name
#     end
#     column :user
#     actions
#   end

#   show do |event|
#     attributes_table do
#       row :title
#       row :user
#       row :description
#       row :location
#       row :hoster do
#         mail_to event.hoster_email, event.hoster_name
#       end
#       row :code
#       row :date do
#         event.date.strftime('%d/%m/%Y')
#       end
#       row :end_date do
#         event.end_date.strftime('%d/%m/%Y')
#       end
#     end
#   end

# end
