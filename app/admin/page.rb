ActiveAdmin.register Page do

  permit_params :title, :body, :contact_form,
    :image_attributes => [:id, :file, :file_cache, :_destroy],
    :sidebar_image_attributes => [:id, :file, :file_cache, :_destroy],
    :attachments_attributes => [:id, :file, :file_cache, :_destroy]

  controller do
    defaults finder: :find_by_slug
  end

  config.filters = false

  index do
    column :title
    actions
  end

  # filter :title
  # filter :body
  # filter :created_at
  # filter :updated_at


  form html: {multipart: true} do |f|
    f.inputs do
      f.input :title
      f.input :body, as: :ckeditor
      f.has_many :attachments, allow_destroy: true, heading: "Attachments" do |fa|
        fa.input :file, hint: fa.object.file.url ? File.basename(fa.object.file.url) : ''
        fa.input :file_cache, as: :hidden
      end
    end
    f.inputs "Contact form" do
      f.input :contact_form
    end
    f.inputs for: :image do |fi|
      fi.input :file, label: "Header Image", hint: image_tag(f.object.image.file.url(:thumb))
      fi.input :file_cache, as: :hidden
      fi.input :_destroy, label: "Delete", as: :boolean
    end
    f.inputs for: :sidebar_image do |fi|
      fi.input :file, label: "Sidebar Image", hint: image_tag(f.object.sidebar_image.file.url(:thumb))
      fi.input :file_cache, as: :hidden
      fi.input :_destroy, label: "Delete", as: :boolean
    end
    f.actions
  end

  show do |page|
    attributes_table do
      # row :id
      row :title
      row :body do
        page.body.html_safe
      end
      row :contact_form do
        status_tag page.contact_form? ? "yes" : "no"
      end
      row :image do
        link_to image_tag(page.image.file.url(:tiny_thumb)), page.image.file.url if page.image.file.file
      end
      row :sidebar_image do
        link_to image_tag(page.sidebar_image.file.url(:tiny_thumb)), page.sidebar_image.file.url if page.sidebar_image.file.file
      end
      row :attachments do
        attachments = page.attachments.map { |attachment| link_to File.basename(attachment.file.url), attachment.file.url }
        attachments.empty? ? '' : content_tag(:ul, attachments.map { |a| content_tag :li, a }.join('').html_safe)
      end
      row :path do
        link_to page.slug, page_path(page)
      end
      row :created_at
      row :updated_at
    end
  end

end
