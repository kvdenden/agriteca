ActiveAdmin.register Block do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end

ActiveAdmin.register Block do

  actions :index, :show, :update, :edit


  permit_params :title, :body

  config.filters = false

  index do
    column :title
    actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :body, as: :ckeditor
    end
    f.actions
  end

  show do |block|
    attributes_table do
      row :title
      row :body do
        block.body.html_safe
      end
    end
  end

end
