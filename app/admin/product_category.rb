ActiveAdmin.register ProductCategory, as: "Category" do

  permit_params :title, :primary,
    :image_attributes => [:id, :file, :file_cache, :_destroy]

  menu parent: "Products"

  controller do
    defaults finder: :find_by_slug
  end

  config.filters = false

  config.sort_order = 'position_asc'
  config.paginate = false

  sortable tree: true

  index as: :sortable do
    label :title
    actions
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :primary
    end
    f.inputs for: :image do |fi|
      fi.input :file, label: "Image", hint: image_tag(f.object.image.file.url(:thumb))
    end
    f.actions
  end

  show do |category|
    attributes_table do
      row :title
      row :image do
        link_to(image_tag(category.image.file.url(:tiny_thumb)), category.image.file.url).html_safe
      end
      row :primary do
        status_tag category.primary? ? "yes" : "no"
      end
      row :products do
        products = category.products.map { |product| link_to product.title, admin_product_path(product) }
        products.empty? ? '' : content_tag(:ul, products.map { |p| content_tag :li, p }.join('').html_safe)
      end
      row :path do
        link_to products_path(category.slug), products_path(category.slug)
      end
      row :created_at
      row :updated_at
    end
  end

end
