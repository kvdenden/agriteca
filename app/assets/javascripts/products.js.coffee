# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

init_gallery = (selector) ->
	$(selector).magnificPopup
		type: 'image'
		gallery:
			enabled: true

$(document).ready () ->
	init_gallery(".product-images a")