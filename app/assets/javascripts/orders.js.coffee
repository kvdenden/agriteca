# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready () ->

	console.log("hello world")

	$delivery_checkbox = $("#new_order #order_other_delivery_address")
	$delivery_fieldset = $("#new_order #delivery_address")

	update_delivery_address = (value) ->
		if value
			$delivery_fieldset.show()
		else
			$delivery_fieldset.hide()

	$delivery_checkbox.change () ->
		console.log("I am here...")
		console.log($(this).is(":checked"))
		update_delivery_address($(this).is(":checked"))

	update_delivery_address($delivery_checkbox.is(":checked"))