# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

update_cart_item = (id, quantity) ->
	$.post "cart/update/#{id}/#{quantity}", () -> location.reload()

$(document).ready () ->
	$("#cart-items input.cart-item-quantity").each (idx, el) ->
		original_value = $(el).val()
		$(el).typing
			stop: (evt, $el) ->
				id = $el.data("cart-item")
				quantity = $el.val()
				if $.isNumeric(quantity) && parseInt(quantity) >= 1
					update_cart_item(id, quantity)
				else
					$el.val(original_value)