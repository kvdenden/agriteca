class Variation < ActiveRecord::Base
  belongs_to :attribute, touch: true

  validates_presence_of :title, :attribute
end
