class Contact < MailForm::Base
  attribute :subject,     :validate => true
  attribute :first_name,  :validate => true
  attribute :last_name,   :validate => true
  attribute :location
  attribute :email,       :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :message,     :validate => true

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
      :subject => subject,
      :from => %("#{first_name} #{last_name}" <#{email}>),
      :to => 'info@agriteca.com'
    }
  end
end