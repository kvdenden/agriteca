class NewsItem < ActiveRecord::Base

  has_one :image, as: :imageable, dependent: :destroy
  accepts_nested_attributes_for :image, reject_if: lambda { |i| i[:file].blank? && i[:file_cache].blank? }, allow_destroy: true

  validates_presence_of :title

  after_initialize :init_image, :if => :new_record?

  default_scope { order('created_at DESC') }

  def init_image
    self.image ||= Image.new
  end

  def teaser
    HTML_Truncator.truncate(self.body, 50)
  end
end
