class Order < ActiveRecord::Base
  has_many :line_items, dependent: :destroy
  belongs_to :event
  belongs_to :coupon
  has_one :invoice, dependent: :destroy

  before_save :update_delivery_address
  before_save :calc_shipping_info

  enumerate :country
  enumerate :delivery_country, with: Country
  enumerate :from_country, with: Country
  enumerate :shipping_type

  scope :paid, -> { where(paid: true) }
  scope :unpaid, -> { where(paid: false) }

  # attr_reader :code

  before_create :set_reference

  validates_presence_of :first_name, :name, :email, :address1, :postal_code, :city, :country, :phone_number
  validates_presence_of :delivery_name, :delivery_address1, :delivery_postal_code, :delivery_city, :delivery_country, if: :other_delivery_address
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }
  validates :vat_number, :valvat => {:allow_blank => true}
  validates :terms_of_service, acceptance: true
  # validate :validate_code

  # def validate_code
  #   if Event.find_by_code(code).nil?
  #     errors.add(:code, I18n.t('invalid_code'))
  #   end
  # end

  def code
    self.event ? self.event.code : nil
  end

  def code=(code)
    event = Event.find_by_code(code)
    self.event = event
  end

  def coupon_code
    self.coupon ? self.coupon.code : nil
  end

  def coupon_code=(coupon_code)
    coupon = Coupon.find_by_code(coupon_code)

    self.coupon = coupon if coupon && !coupon.used?
  end

  def discount
    self.coupon ? ([coupon.value, total_with_vat].min) : 0
  end

  def set_reference
    self.reference = SecureRandom.hex
  end

  def full_name
    "#{first_name} #{name}"
  end

  def full_address
    "#{address2.blank? ? address1.to_s : address1.to_s + ' ' + address2.to_s}, #{postal_code} #{city}, #{country(:name)}"
  end

  def full_delivery_address
    "#{delivery_address2.blank? ? delivery_address1.to_s : delivery_address1.to_s + ' ' + delivery_address2.to_s}, #{delivery_postal_code} #{delivery_city}, #{delivery_country(:name)}"
  end

  def subtotal
    line_items.inject(0) { |sum, i| sum += (i.price * i.quantity) }
  end

  def total_without_vat
    subtotal / (1 + vat_percentage)
  end

  def vat_percentage
    0.21
  end

  def vat
    subtotal - total_without_vat
  end

  def total_with_vat
    calc_shipping_info unless shipping_cost
    (vat_payable? ? subtotal : total_without_vat) + shipping_cost - discount
  end

  def total_in_cents
    (total_with_vat * 100).to_i
  end

  def vat_payable?
    vat_number.blank? || country == Country[:netherlands]
  end

  def weight
    line_items.inject(0) { |sum, i| sum += (i.product.weight * i.quantity) }
  end

  def ships_from_belgium
    line_items.all? { |i| i.product.ships_from_belgium }
  end

  def update_delivery_address
    unless self.other_delivery_address
      self.delivery_name = "#{self.first_name} #{self.name}"
      self.delivery_address1 = self.address1
      self.delivery_address2 = self.address2
      self.delivery_country = self.country
      self.delivery_city = self.city
      self.delivery_postal_code = self.postal_code
    end
  end

  def calc_shipping_info
    if self.delivery_country?
      # set from_country
      be = Country[:belgium]
      nl = Country[:netherlands]

      if self.delivery_country != be || !self.ships_from_belgium
        self.from_country = nl
      else
        self.from_country = be
      end

      to_country = self.country
      to_country = be if self.from_country == nl && to_country != nl

      w = self.weight

      letter = ShippingType[:letter]
      parcel = ShippingType[:parcel]

      sc = nil

      if line_items.all? { |i| i.product.shipping_type == letter }
        sc = ShippingCost.where("
          from_country = ? AND
          to_country = ? AND
          shipping_type = ? AND
          max_weight >= ?", self.from_country, to_country, letter, w).
        order(max_weight: :asc).first
      end

      if sc.nil?
        sc = ShippingCost.where("
          from_country = ? AND
          to_country = ? AND
          shipping_type = ? AND
          max_weight >= ?", self.from_country, to_country, parcel, w).
        order(max_weight: :asc).first
      end

      if sc
        # set shipping_type and shipping_cost
        self.shipping_type = sc.shipping_type
        self.shipping_cost = sc.cost
      else
        # TODO: error, no shipping cost found
        self.shipping_cost = 0
      end

    else
      # TODO: error, country must be filled in
      self.shipping_cost = 0
    end
  end

  def generate_invoice
    self.build_invoice unless self.invoice
    save
  end

end
