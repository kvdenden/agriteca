class Country < ActiveEnum::Base
  value id: 1,  name: "Belgium", country_code: "BE"
  value id: 2,  name: "Netherlands", country_code: "NL"
  value id: 3,  name: "Denmark", country_code: "DK"
  value id: 4,  name: "Germany", country_code: "DE"
  value id: 5,  name: "France", country_code: "FR"
  value id: 6,  name: "Italy", country_code: "IT"
  value id: 7,  name: "Luxembourg", country_code: "LU"
  value id: 8,  name: "Austria", country_code: "AT"
  value id: 9,  name: "Spain", country_code: "ES"
  value id: 10, name: "UK", country_code: "UK"
  value id: 11, name: "Sweden", country_code: "SE"
end
