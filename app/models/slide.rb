class Slide < ActiveRecord::Base
  acts_as_list
  default_scope { order('position ASC') }

  has_one :image, as: :imageable, dependent: :destroy
  accepts_nested_attributes_for :image, allow_destroy: true

  after_initialize :init_image, :if => :new_record?

  validates_associated :image

  def init_image
    self.image ||= Image.new
  end
end
