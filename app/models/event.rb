class Event < ActiveRecord::Base
  belongs_to :user
  has_many :orders
  has_one :coupon
  has_one :commission_invoice

  scope :between, lambda { |start_time, end_time| where(date: Time.at(start_time.to_i)..Time.at(end_time.to_i)) }
  scope :ended, lambda { where 'end_date <= ?', Time.now }

  just_define_datetime_picker :date
  just_define_datetime_picker :end_date

  after_initialize :set_date

  after_create :generate_code

  validates_presence_of :user, :title, :date

  def self.generate_coupons
    # this method runs every day

    # check if there are events that have ended and that do not have a coupon yet
    events = Event.ended.where coupon: nil
    events.each do |e|
      e.generate_coupon
    end
  end

  def self.generate_commission_invoices
    # this method runs every day

    # check if there are events that have ended and that do not have a commission invoice yet
    events = Event.ended.where commission_invoice: nil
    events.each do |e|
      e.generate_commission_invoice if e.user.profile.gets_commission?
    end
  end

  def set_date
    self.date ||= Time.now
    self.end_date ||= Time.now + 1.day
  end

  def generate_code
    self.code = self.user.name.split(" ").map{|s|s.first.upcase}.join("") + '-' + SecureRandom.hex(2).upcase + '-' + self.id.to_s
    save
  end

  def generate_coupon
    # get total of all (regular) orders
    event_orders = self.orders.paid.where created_at: (self.date..self.end_date)
    event_order_total = event_orders.inject(0) { |sum,o| sum+o.subtotal }

    # generate coupon code
    code = SecureRandom.hex(8).upcase
    self.build_coupon code: code, value: event_order_total * 0.1
    save

    # check if hoster info is available
    if self.hoster_email?
      CouponMailer.new_coupon(self.coupon).deliver # mail to user
    end

  end

  def generate_commission_invoice
    event_orders = self.orders.paid.where created_at: (self.date..self.end_date)
    event_order_total = event_orders.inject(0) { |sum,o| sum+o.total_with_vat }
    self.build_commission_invoice total: event_order_total * 0.2, user: self.user, year: Date.today.year, invoice_type: 'event'
    save
    InvoiceMailer.invoice(self.commission_invoice).deliver
  end

  def ended?
    self.end_date <= Time.now
  end

  def as_json(options = {})
    {
      :id => self.id,
      :title => self.title,
      :description => self.description || '',
      :start => self.date.rfc822,
      :end => self.date.rfc822,
      :url => Rails.application.routes.url_helpers.event_path(id)
    }
  end

  def self.format_date(date_time)
    Time.at(date_time.to_i).to_formatted_s(:db)
  end
end
