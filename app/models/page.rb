class Page < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  # has_many :images, as: :imageable, dependent: :destroy
  # accepts_nested_attributes_for :images, reject_if: lambda { |i| i[:file].blank? && i[:file_cache].blank? }, allow_destroy: true

  has_one :image, as: :imageable, dependent: :destroy
  accepts_nested_attributes_for :image, allow_destroy: true

  has_one :sidebar_image, as: :imageable, dependent: :destroy, class_name: Ad
  accepts_nested_attributes_for :sidebar_image, allow_destroy: true

  has_many :attachments, as: :attachable, dependent: :destroy
  accepts_nested_attributes_for :attachments, reject_if: lambda { |i| i[:file].blank? && i[:file_cache].blank? }, allow_destroy: true

  validates_presence_of :title

  after_initialize :init_image

  def init_image
    self.image ||= Image.new
    self.sidebar_image ||= Ad.new
  end

  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end

end
