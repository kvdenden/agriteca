class ShippingType < ActiveEnum::Base
  value id: 1, name: "letter"
  value id: 2, name: "parcel"
end