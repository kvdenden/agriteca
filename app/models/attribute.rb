class Attribute < ActiveRecord::Base
  has_many :variations, inverse_of: :attribute, dependent: :destroy
  has_and_belongs_to_many :products, join_table: :product_attributes

  accepts_nested_attributes_for :variations, reject_if: lambda { |i| i[:title].blank? }, allow_destroy: true

  validates_presence_of :title
end
