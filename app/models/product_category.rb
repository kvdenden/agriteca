class ProductCategory < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  default_scope { order('position ASC') }

  scope :primary, -> { where(primary: true) }

  has_ancestry orphan_strategy: :adopt

  has_many :products, foreign_key: :category_id

  has_one :image, as: :imageable, dependent: :destroy
  accepts_nested_attributes_for :image, allow_destroy: true

  after_initialize :init_image, :if => :new_record?

  validates_presence_of :title

  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end

  def init_image
    self.image ||= Image.new
  end

end
