class Profile < ActiveRecord::Base
  belongs_to :user
  belongs_to :parent, class_name: User

  enumerate :country

  def name
    "#{first_name} #{last_name}"
  end

  def full_address
    "#{address2.blank? ? address1.to_s : address1.to_s + ' ' + address2.to_s}, #{postal_code} #{city}, #{country(:name)}"
  end
end
