class CommissionInvoice < ActiveRecord::Base
  belongs_to :user
  belongs_to :event

  before_create :generate_code

  after_create :generate_pdf

  before_destroy :delete_pdf

  def generate_code
    number = CommissionInvoice.where(year: self.year).count + 1
    self.code = "#{year}#{number.to_s.rjust(8, '0')}"
  end

  def generate_pdf
    controller = ApplicationController.new
    pdf = controller.render_to_string(
      :pdf => "invoice",
      :page_size => 'A4',
      :template => "invoices/commission/#{self.invoice_type}.pdf.erb",
      :layout => 'layouts/invoice',
      :header => {
        :html => {
          :template => 'invoices/_header.pdf.erb'
        }
      },
      :footer => {
        :html => {
          :template => 'invoices/_footer.pdf.erb'
        }
      },
      :locals => { :invoice => self }
    )

    File.open(self.filename, 'wb') do |file|
      file << pdf
    end
  end

  def delete_pdf
    File.delete(self.filename) if File.exist?(self.filename)
  end

  def filename
    self.code ? Rails.public_path.join('invoices', 'commission', "#{self.code}.pdf") : nil
  end
end
