class ShippingCost < ActiveRecord::Base
  enumerate :from_country, with: Country
  enumerate :to_country, with: Country
  enumerate :shipping_type, with: ShippingType

  validates_presence_of :from_country, :to_country, :shipping_type, :max_weight, :cost
end
