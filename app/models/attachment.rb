class Attachment < ActiveRecord::Base
  belongs_to :attachable, polymorphic: true, touch: true

  mount_uploader :file, AttachmentUploader

  validates_presence_of :file
end
