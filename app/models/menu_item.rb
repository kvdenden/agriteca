class MenuItem < ActiveRecord::Base

  has_ancestry orphan_strategy: :adopt

  default_scope { order('position ASC') }

  validates_presence_of :title

end
