class QuickLink < ActiveRecord::Base

  default_scope { order('position ASC') }

  validates_presence_of :title, :link

end
