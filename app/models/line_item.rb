class LineItem < ActiveRecord::Base
  belongs_to :product
  belongs_to :order
  has_many :variations, class_name: "LineItemVariation"
end
