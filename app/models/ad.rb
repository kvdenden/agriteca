class Ad < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true, touch: true

  mount_uploader :file, ImageUploader

  # validates_presence_of :file
end
