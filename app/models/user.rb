class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :profile, dependent: :destroy
  has_many :events
  has_many :commission_invoices

  accepts_nested_attributes_for :profile

  scope :approved, -> { where approved: true }
  scope :unapproved, -> { where approved: false }

  after_initialize :init_profile, :if => :new_record?

  before_save :check_status # check if user was just approved

  def self.generate_invoices
    # this method runs on the first of every month

    User.all.each do |u|
      if u.profile.gets_commission?
        month = Date.yesterday.month
        year = Date.yesterday.year
        commission = u.calculate_commission(month, year)
        monthly_commission =
          commission[:back_order_commission] + 
          commission[:referral_commission] + 
          commission[:monthly_bonus] + 
          commission[:group_bonus]

        invoice = u.commission_invoices.build total: monthly_commission, month: month, year: year, invoice_type: 'monthly'
        u.save

        InvoiceMailer.invoice(invoice).deliver
      end
    end
  end

  def name
    profile.name
  end

  def init_profile
    self.profile ||= Profile.new
  end

  def active_for_authentication?
    super && approved?
  end

  def parent
    profile.parent
  end

  def children
    User.joins(:profile).where(profiles: {parent_id: self.id})
  end

  def calculate_commission(month, year)
    date = DateTime.new(year, month)
    # find all orders with event code that belongs to event of this consultant
    all_orders = Order.paid.includes(:event).where created_at: (date.beginning_of_month..date.end_of_month), events: {user_id: self.id}
    # if order is placed before end date of event -> regular order, else -> backorder
    event_orders, back_orders = all_orders.partition { |o| o.created_at < o.event.end_date }

    # 20% commission on all regular orders
    event_order_total = event_orders.inject(0) { |sum, o| sum + o.total_with_vat }
    event_order_commission = event_order_total * 0.2
    # 10% commission on all reorders (20% if total sum is greater than 2000)
    back_order_total = back_orders.inject(0) { |sum, o| sum + o.total_with_vat }
    back_order_commission = back_order_total * (back_order_total > 2000 ? 0.2 : 0.1)
    # variable bonus (4.5 - 7%) on regular orders
    if event_order_total > (100000 / 12.0)
      bonus_percentage = 0.07
    elsif event_order_total > (75000 / 12.0)
      bonus_percentage = 0.065
    elsif event_order_total > (50000 / 12.0)
      bonus_percentage = 0.055
    elsif event_order_total > (40000 / 12.0)
      bonus_percentage = 0.05
    else
      bonus_percentage = 0
    end
    event_order_bonus = event_order_total * bonus_percentage

    child_order_commission = 0
    grandchild_order_commission = 0

    group_bonus_children = []
    # find all children
    self.children.each do |child|
      # for each child, find all event orders for this month
      child_orders = Order.paid.includes(:event).where created_at: (date.beginning_of_month..date.end_of_month), events: {user_id: child.id}
      child_event_orders, child_back_orders = child_orders.partition { |o| o.created_at < o.event.end_date }
      child_event_order_total = child_event_orders.inject(0) { |sum, o| sum + o.total_with_vat }

      group_bonus_children << child_event_order_total if child_event_order_total > 2000
      # 2% commission on orders of children
      child_order_commission += child_event_order_total * 0.05

      # find grandchildren
      child.children.each do |grandchild|
        grandchild_orders = Order.paid.includes(:event).where created_at: (date.beginning_of_month..date.end_of_month), events: {user_id: grandchild.id}
        grandchild_event_orders, grandchild_back_orders = grandchild_orders.partition { |o| o.created_at < o.event.end_date }
        grandchild_event_order_total = grandchild_event_orders.inject(0) { |sum, o| sum + o.total_with_vat }
        # 2% commission on orders of grandchildren
        grandchild_order_commission += grandchild_event_order_total * 0.02
      end
    end

    group_bonus = 0
    if event_order_total > 2000
      group_bonus_total = group_bonus_children.reduce :+
      if group_bonus_children.length >= 6
        group_bonus = group_bonus_total * 0.05 # 5% group bonus
      elsif group_bonus_children.length >= 3
        group_bonus = group_bonus_total * 0.02 # 2% group bonus
      end
    end

    return {
      event_order_commission: event_order_commission,
      back_order_commission: back_order_commission,
      referral_commission: child_order_commission + grandchild_order_commission,
      monthly_bonus: event_order_bonus,
      group_bonus: group_bonus
    }
  end

  # emails
  after_create :send_registration_mail
  def send_registration_mail
    UserMailer.new_user(self).deliver
  end

  def check_status
    UserMailer.user_approved(self).deliver if approved_changed? && approved
  end
end
