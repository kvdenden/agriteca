class Product < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  scope :primary, -> { where(primary: true) }

  # acts_as_taggable
  enumerate :shipping_type

  has_many :images, as: :imageable, dependent: :destroy
  accepts_nested_attributes_for :images, reject_if: lambda { |i| i[:file].blank? && i[:file_cache].blank? }, allow_destroy: true

  belongs_to :category, class_name: "ProductCategory", foreign_key: "category_id"

  has_and_belongs_to_many :options, class_name: "Attribute", join_table: :product_attributes
  # accepts_nested_attributes_for :options

  validates_presence_of :title, :price, :category, :weight, :shipping_type, :code
  validates_uniqueness_of :code

  scope :published, -> { where published: true }
  scope :unpublished, -> { where published: false }

  before_create :set_description

  def image
    self.images.first
  end

  def self.default_image type
    return type ? "#{type.to_s}/product-default.png" : "product-default.png"
  end

  def set_description
    self.description ||= ''
  end

  def should_generate_new_friendly_id?
    slug.blank? || title_changed?
  end
end
