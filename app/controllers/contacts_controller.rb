class ContactsController < ApplicationController

  def new
    @contact = Contact.new
  end

  def submit
    @contact = Contact.new(params[:contact])
    @contact.request = request
    @path = params[:path]
    if @contact.deliver
      flash.now[:error] = nil
      flash[:notice] = t('thank_you_for_your_message')
      redirect_to @path
    else
      # if @path
      #   flash[:error] = 'Cannot send message.'
      #   redirect_to @path
      # else
        flash.now[:error] = t('cannot_send_message')
        render :new
      # end
    end
  end

end
