class InvoicesController < ApplicationController

  def generate_pdf invoice
    pdf = WickedPdf.new.pdf_from_string(
       render_to_string(
         :pdf => "invoice",
         :template => '/invoices/show.pdf.erb',
         :locals => { '@invoice' => invoice }
       )
    )

    File.open(invoice.filename, 'wb') do |file|
      file << pdf
    end
  end
end
