class CartController < ApplicationController

  before_filter :get_cart

  def add_item
    @product = Product.find params[:product_id]

    if @product
      line_item = LineItem.new product: @product, price: @product.price
      line_item.quantity = params[:quantity] || 1
      params[:options].each do |option_id, variation_id|
        option = Attribute.find(option_id)
        variation = Variation.find(variation_id)
        line_item.variations.build option: option.title, variation: variation.title
      end if params[:options]

      @cart.add line_item, line_item.price, line_item.quantity
    end
    flash[:success] = t('item_added')
    redirect_to @product
  end

  def remove_item
    cart_item = ShoppingCartItem.find params[:id]
    cart_item.delete

    redirect_to cart_path
  end

  def update_item
    cart_item = ShoppingCartItem.find params[:id]
    quantity = params[:quantity].to_i
    if quantity >= 1
      cart_item.update_quantity params[:quantity]
      cart_item.item.quantity = params[:quantity]
    else
      cart_item.delete
    end

    redirect_to cart_path
  end

  def show
  end

end
