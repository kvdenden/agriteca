class OrdersController < ApplicationController

  # cardgate settings
  @@hash_key = 'c3xkCX9h'
  @@siteid = '4952'
  @@test = false

  before_filter :get_cart, only: [:new, :create]

  def new
    if @cart.empty?
      redirect_to cart_path
    else
      @order = Order.new
      @other_delivery_address = false
    end
  end

  def create
    # event = Event.find_by_code(code)
    @other_delivery_address = params[:other_delivery_address]

    @order = Order.new(order_parameters)
    @cart.shopping_cart_items.each do |cart_item|
      line_item = cart_item.item
      @order.line_items << line_item
    end
    @order.calc_shipping_info
    if @order.save
      @cart.clear
      redirect_to pay_order_path(@order)
    else
      render "new"
    end
  end

  def show
    @order = Order.find(params[:id])
  end

  def pay
    @test = @@test ? '1' : '0'
    @siteid = @@siteid
    @order = Order.find(params[:id])
    @description = ''
    @hash = calc_hash(@order)
    @cardgate_url = 'https://gateway.cardgateplus.com/'
  end

  def success
    @order = Order.find(params[:id])
    flash[:success] = t('order_success')
    OrderMailer.order_success(@order).deliver
    redirect_to @order
  end

  def failed
    @order = Order.find(params[:id])
    flash[:error] = t('order_failed')
    redirect_to pay_order_path(@order)
  end

  def callback
    # todo: check hash
    transaction_id = params[:transaction_id]
    ref = params[:ref]
    status = params[:status].to_i
    payment_method = params[:billing_option]

    @order = Order.find_by_reference(ref)
    if @order
      unless @order.status == 200
        @order.update_attribute :status, status
        @order.update_attribute :payment_method, payment_method
        if status == 200
          @order.generate_invoice
          @order.update_attribute :paid, true
          OrderMailer.order_confirmation(@order).deliver # mail to user
          OrderMailer.order_details(@order).deliver # mail to admin
        end
      end
    end
    render text: "#{transaction_id}.#{status}"
  end

  private
  def order_parameters
    params.require(:order).permit(:first_name, :name, :email, :address1, :address2, :country, :city, :postal_code, :phone_number, :code, :coupon_code,
      :other_delivery_address, :delivery_name, :delivery_address1, :delivery_address2, :delivery_country, :delivery_city, :delivery_postal_code, :vat_number, :terms_of_service)
  end

  def calc_hash(order)
    elements = [@@siteid, order.total_in_cents, order.reference, @@hash_key]
    elements.unshift 'TEST' if @@test
    Digest::MD5.hexdigest(elements.join)
  end

end
