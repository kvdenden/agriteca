class EventsController < ApplicationController
  load_and_authorize_resource
  skip_authorize_resource :only => :show

  # before_filter :require_login, except: :show

  def index
    @events = current_user.events
    @events = current_user.events.between(params['start'], params['end']) if (params['start'] && params['end'])
    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @events }
    end
  end

  def show
    # @event = current_user.events.find(params[:id])
    @event = Event.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @event }
    end
  end

  def new
    @event = current_user.events.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @event }
    end
  end

  def create
    @event = current_user.events.new(event_parameters)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, :notice => 'Event was successfully created.' }
        format.json { render :json => @event, :status => :created, :location => @event }
      else
        format.html { render :action => "new" }
        format.json { render :json => @event.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit
    @event = current_user.events.find(params[:id])

    respond_to do |format|
      format.html # edit.html.erb
      format.json { render :json => @event }
    end
  end

  def update
    @event = current_user.events.find(params[:id])

    respond_to do |format|
      if @event.update(event_parameters)
        format.html { redirect_to @event, :notice => 'Event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @event.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @event = current_user.events.find(params[:id])
    @event.destroy

    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
    end
  end

  private
  def require_login
    unless user_signed_in?
      flash[:error] = t('login_required')
      redirect_to new_user_session_path
    end
  end

  def event_parameters
    params.require(:event).permit(:title, :date, :description, :location, :hoster_name, :hoster_email)
  end
end
