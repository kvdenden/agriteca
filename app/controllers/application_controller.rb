class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_filter :init_parameters

  # after_filter do
  #   I18n.exception_handler.store_missing_translations unless Rails.env.production?
  # end

  protected
  def get_cart
    cart_id = session[:cart_id]
    begin
      @cart = cart_id ? ShoppingCart.find(cart_id) : ShoppingCart.create
    rescue ActiveRecord::RecordNotFound
      @cart = ShoppingCart.create
    end
    session[:cart_id] = @cart.id
  end

  def init_parameters
    resource = controller_name.singularize.to_sym
    method = "#{resource}_parameters"
    params[resource] &&= send(method) if respond_to?(method, true)
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:email, :password, :password_confirmation, profile_attributes: [:first_name, :last_name, :address1, :address2, :country, :city, :postal_code, :phone_number, :company_number])
    end
  end
end
