class NewsController < ApplicationController

  def show
    @news_items = NewsItem.limit(5)
    @item = NewsItem.find(params[:id])
  end

  def archive
    page = params[:page] || 0
    @news_items = NewsItem.page(page)
  end

end
