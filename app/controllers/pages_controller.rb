class PagesController < ApplicationController

  def index
    @slides = Slide.all
    @categories = ProductCategory.primary
    @news_items = NewsItem.limit(5)
    @block = Block.first
  end

  def show
    @page = Page.friendly.find(params[:slug])
    @news_items = NewsItem.limit(5)
    @contact = Contact.new subject: @page.title
  end
end
