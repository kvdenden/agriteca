class ProductsController < ApplicationController

  def index
    @category = nil
    if params[:category]
      begin
        @category = ProductCategory.friendly.find(params[:category])
        @products = Product.primary.where category_id: @category.subtree_ids, published: true
        @related_products = Product.where primary: false, category_id: @category.subtree_ids, published: true
      rescue ActiveRecord::RecordNotFound
        redirect_to products_url
      end
    else
      @products = Product.primary.where published: true
      @related_products = Product.where primary: false, published: true
    end
  end

  def show
    begin
      @product = Product.friendly.find(params[:slug])
      @category = @product.category
      @related_products = Product.where primary: false, category_id: @category.subtree_ids, published: true
    rescue
      redirect_to products_url
    end
  end

end
