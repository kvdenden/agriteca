class CouponMailer < ActionMailer::Base
  default from: "noreply@agriteca.com"

  @@admin_email = "info@agriteca.com"

  def new_coupon(coupon)
    @event = coupon.event
    mail to: @event.hoster_email, subject: t('new_coupon')
  end

end
