class OrderMailer < ActionMailer::Base
  default from: "noreply@agriteca.com"

  @@admin_email = "bestelling@agriteca.com"

  def order_success(order)
    @order = order
    mail to: @order.email, subject: t('order_success')
  end

  def order_confirmation(order)
    @order = order
    attachments['invoice.pdf'] = File.read(@order.invoice.filename)
    mail to: @order.email, subject: t('order_confirmation')
  end

  def order_details(order)
    @order = order
    attachments['invoice.pdf'] = File.read(@order.invoice.filename)
    mail to: @@admin_email, subject: t('new_order')
  end
end
