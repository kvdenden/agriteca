class InvoiceMailer < ActionMailer::Base
  default from: "noreply@agriteca.com"

  def invoice(invoice)
    @invoice = invoice
    attachments['invoice.pdf'] = File.read(@invoice.filename)
    mail to: @invoice.user.email, subject: t('commission_invoice')
  end

end
