class UserMailer < ActionMailer::Base
  default from: "noreply@agriteca.com"

  @@admin_email = "info@agriteca.com"

  def new_user(user)
    @user = user
    mail to: @@admin_email, subject: t('new_user')
  end

  def user_approved(user)
    @user = user
    mail to: @user.email, subject: t('account_approved')
  end
end
