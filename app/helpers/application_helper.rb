module ApplicationHelper

  def turbolinks?
    if (params[:controller] == 'products' and params[:action] == 'show') or
       (params[:controller] == 'pages' and params[:action] == 'index')
      return false
    else
      return true
    end
  end

  def active_menu_item
    return MenuItem.find_by_link request.path
  end

  def active_menu_item? menu_item, path=request.path
    return menu_item.link == path
  end

  def main_menu
    items_hash = MenuItem.arrange order: :position
    return items_hash.map { |item, children| menu_item_html(item, children, true) }.join("").html_safe
  end

  def top_menu
    cart_id = session[:cart_id]
    begin
      cart = cart_id ? ShoppingCart.find(cart_id) : ShoppingCart.new
    rescue ActiveRecord::RecordNotFound
      cart = ShoppingCart.new
    end

    menu_html = ""
    menu_html += content_tag :li, link_to(t('shopping_cart_quantity', quantity: cart.shopping_cart_items.inject(0){|i,j|i+j.quantity}), cart_path)
    menu_html += content_tag :li, link_to(t('checkout'), checkout_path)
    return menu_html.html_safe
  end

  def sidebar_menu
    begin
      items_hash = active_menu_item.root.subtree.arrange order: :position
      return items_hash.map { |item, children| menu_item_html(item, children) }.join("").html_safe
    rescue
    end
  end

  def product_menu category=nil
    # items_hash = category ? (category.root.subtree.arrange order: :position) : (ProductCategory.arrange order: :position)
    items_hash = ProductCategory.arrange order: :position
    return items_hash.map { |item, children| category_menu_html(item, children) }.join("").html_safe
  end

  def quick_links
    QuickLink.all.map { |l| content_tag :li, link_to(l.title, l.link) }.join("").html_safe
  end

  def shopping_cart
    cart_id = session[:cart_id]
    begin
      cart = cart_id ? ShoppingCart.find(cart_id) : ShoppingCart.new
    rescue ActiveRecord::RecordNotFound
      cart = ShoppingCart.new
    end


    cart_body = ""
    if cart.empty?
      cart_body += content_tag :p, t("empty_shopping_cart")
    else
      cart_body += content_tag :p, t("total_price", price: number_to_currency(cart.total, unit: '€'))
      cart_body += content_tag :div, link_to(t("view_shopping_cart"), cart_path), class: "text-right"
    end
    return cart_body.html_safe
  end

  def bootstrap_class_for flash_type
    case flash_type
      when :success
        "alert-success" # Green
      when :error
        "alert-danger" # Red
      when :alert
        "alert-warning" # Yellow
      when :notice
        "alert-info" # Blue
      else
        flash_type.to_s
    end
  end

  def body_classes
    "controller-#{controller.controller_name} action-#{controller.action_name} " + (current_page?(root_url) ? "front" : "not-front")
  end

  private

  def menu_item_html menu_item, children, dropdown=false
    html = ""
    link = get_link menu_item, children
    html += link ? link_to(menu_item.title, link) : menu_item.title
    has_children = !children.empty?
    classes = []
    classes << "dropdown" if dropdown && has_children
    classes << "active" if active_menu_item? menu_item
    if has_children
      # html += content_tag :span, "", class: "caret"
      html += content_tag :ul, children.map { |i, ch| menu_item_html(i, ch) }.join("").html_safe, class: ("dropdown-menu" if dropdown)
    end
    return content_tag :li, html.html_safe, class: classes.join(" ")
  end

  def get_link menu_item, children=nil
    return nil if menu_item.nil?

    if menu_item.link?
      return menu_item.link
    else
      children.empty? ? nil : get_link(*children.first)
    end
  end

  def category_menu_html category, children
    html = ""
    html += link_to category.title, products_path(category)
    has_children = !children.empty?
    if has_children
      # html += content_tag :span, "", class: "caret"
      html += content_tag :ul, children.map { |i, ch| category_menu_html(i, ch) }.join("").html_safe
    end
    return content_tag :li, html.html_safe, class: ("active" if products_path(category) == request.path)
  end
end
