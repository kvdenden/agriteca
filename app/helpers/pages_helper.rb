module PagesHelper

  def attachments page
    page.attachments.map {|a| content_tag :li, link_to(basename(a.file.to_s), a.file.to_s, target: :_blank) }.join("")
  end

  private

  def basename str
    Pathname.new(str).basename
  end
end
