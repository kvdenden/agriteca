module ProductsHelper

  def product_image product, type=nil
    product.image ? product.image.file.url(type) : Product.default_image(type)
  end
end
