# I18n.exception_handler = I18n::Workflow::ExceptionHandler.new unless Rails.env.production?
# I18n.backend.class.send(:include, I18n::Backend::Fallbacks)
# I18n.backend.class.send(:include, I18n::Backend::Cascade)
# I18n.backend.class.send(:include, I18n::Workflow::AlwaysCascade)
# I18n.backend.class.send(:include, I18n::Workflow::ExplicitScopeKey)

# # Make sure to disable raising error in ActionView:
# Agriteca::Application.config.action_view.raise_on_missing_translations = false