# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Agriteca::Application.config.secret_key_base = 'b37a051ccc803ae63311642b85fa6d2291e672162bb4b04db2ad7e4a9114d2387f848dbd3758f683b5a2bccb775135c610445c7575135e523c1b60c517c835a3'
