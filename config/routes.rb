Agriteca::Application.routes.draw do

  devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  mount Ckeditor::Engine => '/ckeditor'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'pages#index'

  # webshop
  get 'shop(/:category)' => 'products#index', :as => 'products'
  get 'product/:slug' => 'products#show', :as => 'product'

  # shopping cart
  post 'cart/add' => 'cart#add_item', :as => 'add_to_cart'
  post 'cart/remove/:id' => 'cart#remove_item', :as => 'remove_from_cart'
  post 'cart/update/:id/:quantity' => 'cart#update_item', :as => 'update_cart_item'
  get 'cart' => 'cart#show', :as => 'cart'

  # orders
  get 'checkout' => 'orders#new', :as => 'checkout'
  post 'orders' => 'orders#create', :as => 'orders'
  get 'orders/:id' => 'orders#show', :as => 'order'
  get 'orders/:id/pay' => 'orders#pay', :as => 'pay_order' # dummy page (to be replaced by offsite payment gateway)
  get 'orders/:id/success' => 'orders#success', :as => 'order_success' # cardgate return url
  get 'orders/:id/failed' => 'orders#failed', :as => 'order_failed' # cardgate return url failed
  post 'orders/callback' => 'orders#callback', :as => 'order_callback' # cardgate control url (callback)

  # news items

  get 'news/archive' => 'news#archive', :as => 'news_archive'
  get 'news/:id' => 'news#show', :as => 'news_item'

  # events
  resources :events

  # contact form
  get 'contact_form' => 'contacts#new', :as => 'contact_form'
  post 'contact_form' => 'contacts#submit', :as => 'submit_contact_form'

  # basic pages
  get ':slug' => 'pages#show', :as => 'page'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
