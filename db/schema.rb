# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141128112328) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "ads", force: true do |t|
    t.string   "file"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ads", ["imageable_id", "imageable_type"], name: "index_ads_on_imageable_id_and_imageable_type"

  create_table "attachments", force: true do |t|
    t.string   "file"
    t.integer  "attachable_id"
    t.string   "attachable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "attachments", ["attachable_id", "attachable_type"], name: "index_attachments_on_attachable_id_and_attachable_type"

  create_table "attributes", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "blocks", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"

  create_table "commission_invoices", force: true do |t|
    t.string   "code"
    t.integer  "user_id"
    t.integer  "month"
    t.integer  "year"
    t.string   "invoice_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_id"
    t.decimal  "total",        precision: 10, scale: 2
  end

  add_index "commission_invoices", ["event_id"], name: "index_commission_invoices_on_event_id"
  add_index "commission_invoices", ["user_id"], name: "index_commission_invoices_on_user_id"

  create_table "coupons", force: true do |t|
    t.integer  "event_id"
    t.string   "code"
    t.decimal  "value",      precision: 10, scale: 2
    t.boolean  "used"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "coupons", ["event_id"], name: "index_coupons_on_event_id"

  create_table "events", force: true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "description"
    t.datetime "date"
    t.string   "location"
    t.string   "hoster_name"
    t.string   "hoster_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code"
    t.datetime "end_date"
  end

  add_index "events", ["code"], name: "index_events_on_code"
  add_index "events", ["user_id"], name: "index_events_on_user_id"

  create_table "images", force: true do |t|
    t.string   "file"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "images", ["imageable_id", "imageable_type"], name: "index_images_on_imageable_id_and_imageable_type"

  create_table "invoices", force: true do |t|
    t.string   "code"
    t.integer  "order_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "year"
  end

  add_index "invoices", ["order_id"], name: "index_invoices_on_order_id"

  create_table "line_item_variations", force: true do |t|
    t.integer  "line_item_id"
    t.string   "option"
    t.string   "variation"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "line_item_variations", ["line_item_id"], name: "index_line_item_variations_on_line_item_id"

  create_table "line_items", force: true do |t|
    t.integer  "product_id"
    t.decimal  "price",      precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quantity",                            default: 1
    t.integer  "order_id"
  end

  add_index "line_items", ["order_id"], name: "index_line_items_on_order_id"
  add_index "line_items", ["product_id"], name: "index_line_items_on_product_id"

  create_table "menu_items", force: true do |t|
    t.string   "title"
    t.string   "link"
    t.integer  "position"
    t.string   "ancestry"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news_items", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.string   "name"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "postal_code"
    t.integer  "country"
    t.string   "phone_number"
    t.decimal  "shipping_cost",          precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email"
    t.boolean  "paid",                                            default: false
    t.integer  "shipping_type"
    t.integer  "from_country"
    t.integer  "event_id"
    t.string   "reference"
    t.integer  "status"
    t.string   "first_name"
    t.string   "payment_method"
    t.boolean  "other_delivery_address",                          default: false
    t.string   "delivery_name"
    t.string   "delivery_address1"
    t.string   "delivery_address2"
    t.string   "delivery_city"
    t.string   "delivery_postal_code"
    t.integer  "delivery_country"
    t.integer  "coupon_id"
    t.string   "vat_number"
  end

  add_index "orders", ["coupon_id"], name: "index_orders_on_coupon_id"
  add_index "orders", ["event_id"], name: "index_orders_on_event_id"
  add_index "orders", ["reference"], name: "index_orders_on_reference", unique: true

  create_table "pages", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.boolean  "contact_form", default: false
  end

  add_index "pages", ["slug"], name: "index_pages_on_slug"

  create_table "product_attributes", id: false, force: true do |t|
    t.integer "product_id",   null: false
    t.integer "attribute_id", null: false
  end

  add_index "product_attributes", ["product_id"], name: "index_product_attributes_on_product_id"

  create_table "product_categories", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.integer  "position"
    t.string   "ancestry"
    t.boolean  "primary"
  end

  add_index "product_categories", ["ancestry"], name: "index_product_categories_on_ancestry"
  add_index "product_categories", ["slug"], name: "index_product_categories_on_slug"

  create_table "products", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.decimal  "price",              precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.integer  "category_id"
    t.integer  "shipping_type"
    t.integer  "weight"
    t.boolean  "ships_from_belgium",                          default: true
    t.boolean  "primary",                                     default: true
    t.boolean  "adult_only"
    t.boolean  "published",                                   default: true
    t.string   "code"
  end

  add_index "products", ["category_id"], name: "index_products_on_category_id"
  add_index "products", ["slug"], name: "index_products_on_slug"

  create_table "profiles", force: true do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "postal_code"
    t.integer  "country"
    t.string   "phone_number"
    t.integer  "parent_id"
    t.boolean  "gets_commission", default: true
    t.string   "company_number"
  end

  add_index "profiles", ["parent_id"], name: "index_profiles_on_parent_id"
  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id"

  create_table "quick_links", force: true do |t|
    t.string   "title"
    t.string   "link"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shipping_costs", force: true do |t|
    t.integer  "from_country"
    t.integer  "to_country"
    t.integer  "shipping_type"
    t.integer  "max_weight"
    t.decimal  "cost",          precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shopping_cart_items", force: true do |t|
    t.integer  "owner_id"
    t.string   "owner_type"
    t.integer  "quantity"
    t.integer  "item_id"
    t.string   "item_type"
    t.float    "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shopping_carts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "slides", force: true do |t|
    t.string   "title"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true

  create_table "tags", force: true do |t|
    t.string "name"
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "approved",               default: false, null: false
  end

  add_index "users", ["approved"], name: "index_users_on_approved"
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "variations", force: true do |t|
    t.string   "title"
    t.integer  "attribute_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "variations", ["attribute_id"], name: "index_variations_on_attribute_id"

end
