class RemoveDescriptionFromSlides < ActiveRecord::Migration
  def change
    remove_column :slides, :description
  end
end
