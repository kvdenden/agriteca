class AddAdultOnlyToProducts < ActiveRecord::Migration
  def change
    add_column :products, :adult_only, :boolean, default: nil
  end
end
