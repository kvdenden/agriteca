class AddCompanyNumberToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :company_number, :string
  end
end
