class CreateVariations < ActiveRecord::Migration
  def change
    create_table :variations do |t|
      t.string :title
      t.references :attribute, index: true

      t.timestamps
    end
  end
end
