class CreateQuickLinks < ActiveRecord::Migration
  def change
    create_table :quick_links do |t|
      t.string :title
      t.string :link
      t.integer :position

      t.timestamps
    end
  end
end
