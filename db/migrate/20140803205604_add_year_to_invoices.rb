class AddYearToInvoices < ActiveRecord::Migration
  def change
    add_column :invoices, :year, :integer
  end
end
