class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.string :address1
      t.string :address2
      t.string :city
      t.string :postal_code
      t.integer :country
      t.string :phone_number
      t.decimal :shipping_cost, precision: 10, scale: 2

      t.timestamps
    end
  end
end
