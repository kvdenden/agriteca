class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.references :event, index: true
      t.string :code
      t.decimal :value, precision: 10, scale: 2
      t.boolean :used

      t.timestamps
    end
  end
end
