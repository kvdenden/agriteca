class CreateBlocks < ActiveRecord::Migration
  def up
    create_table :blocks do |t|
      t.string :title
      t.text :body

      t.timestamps
    end

    Block.create title: "Frontpage", body: ""
  end

  def down
    drop_table :blocks
  end
end
