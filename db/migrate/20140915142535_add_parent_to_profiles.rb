class AddParentToProfiles < ActiveRecord::Migration
  def change
    add_reference :profiles, :parent, index: true
  end
end
