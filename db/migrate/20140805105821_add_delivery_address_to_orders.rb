class AddDeliveryAddressToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :other_delivery_address, :boolean, default: false
    add_column :orders, :delivery_name, :string
    add_column :orders, :delivery_address1, :string
    add_column :orders, :delivery_address2, :string
    add_column :orders, :delivery_city, :string
    add_column :orders, :delivery_postal_code, :string
    add_column :orders, :delivery_country, :integer
  end
end
