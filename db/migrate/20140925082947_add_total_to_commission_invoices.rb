class AddTotalToCommissionInvoices < ActiveRecord::Migration
  def change
    add_column :commission_invoices, :total, :decimal, precision: 10, scale: 2
  end
end
