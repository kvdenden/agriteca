class CreateLineItemVariations < ActiveRecord::Migration
  def change
    create_table :line_item_variations do |t|
      t.references :line_item, index: true
      t.string :option
      t.string :variation

      t.timestamps
    end
  end
end
