class AddShippingInfoToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :shipping_type, :integer
    add_column :orders, :from_country, :integer
  end
end
