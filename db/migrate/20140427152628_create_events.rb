class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :user, index: true
      t.string :title
      t.text :description
      t.datetime :date
      t.string :location
      t.string :hoster_name
      t.string :hoster_email

      t.timestamps
    end
  end
end
