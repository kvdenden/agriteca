class AddPrimaryToProducts < ActiveRecord::Migration
  def change
    add_column :products, :primary, :boolean, default: true
  end
end
