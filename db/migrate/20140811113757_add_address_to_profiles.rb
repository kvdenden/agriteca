class AddAddressToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :address1, :string
    add_column :profiles, :address2, :string
    add_column :profiles, :city, :string
    add_column :profiles, :postal_code, :string
    add_column :profiles, :country, :integer
    add_column :profiles, :phone_number, :string
  end
end
