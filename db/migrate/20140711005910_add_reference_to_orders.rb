class AddReferenceToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :reference, :string
    add_index :orders, :reference, unique: true
  end
end
