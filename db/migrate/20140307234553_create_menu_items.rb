class CreateMenuItems < ActiveRecord::Migration
  def change
    create_table :menu_items do |t|
      t.string :title
      t.string :link
      t.integer :position
      t.string :ancestry, index: true

      t.timestamps
    end
  end
end
