class AddEventToOrders < ActiveRecord::Migration
  def change
    add_reference :orders, :event, index: true
  end
end
