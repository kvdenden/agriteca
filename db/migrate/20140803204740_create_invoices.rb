class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :code
      t.references :order, index: true

      t.timestamps
    end
  end
end
