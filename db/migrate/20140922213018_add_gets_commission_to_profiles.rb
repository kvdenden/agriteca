class AddGetsCommissionToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :gets_commission, :boolean, default: true
  end
end
