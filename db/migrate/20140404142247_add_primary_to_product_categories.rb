class AddPrimaryToProductCategories < ActiveRecord::Migration
  def change
    add_column :product_categories, :primary, :boolean
  end
end
