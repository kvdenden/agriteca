class CreateCommissionInvoices < ActiveRecord::Migration
  def change
    create_table :commission_invoices do |t|
      t.string :code
      t.references :user, index: true
      t.integer :month
      t.integer :year
      t.string :invoice_type

      t.timestamps
    end
  end
end
