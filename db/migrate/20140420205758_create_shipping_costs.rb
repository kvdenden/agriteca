class CreateShippingCosts < ActiveRecord::Migration
  def change
    create_table :shipping_costs do |t|
      t.integer :from_country
      t.integer :to_country
      t.integer :shipping_type
      t.integer :max_weight
      t.decimal :cost, precision: 10, scale: 2

      t.timestamps
    end
  end
end
