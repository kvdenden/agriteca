class AddShippingInfoToProducts < ActiveRecord::Migration
  def change
    add_column :products, :shipping_type, :integer
    add_column :products, :weight, :integer
    add_column :products, :ships_from_belgium, :boolean, default: true
  end
end
