class AddEventToCommissionInvoices < ActiveRecord::Migration
  def change
    add_reference :commission_invoices, :event, index: true
  end
end
