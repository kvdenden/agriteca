# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

## SHIPPING COST

be = Country[:belgium]
nl = Country[:netherlands]

letter = ShippingType[:letter]
parcel = ShippingType[:parcel]

# order from be
ShippingCost.create from_country: be, to_country: be, shipping_type: letter, max_weight:   350, cost:  3.10
ShippingCost.create from_country: be, to_country: be, shipping_type: parcel, max_weight:  2000, cost:  6.20
ShippingCost.create from_country: be, to_country: be, shipping_type: parcel, max_weight: 10000, cost:  8.30

ShippingCost.create from_country: nl, to_country: be, shipping_type: letter, max_weight:   250, cost:  5.25
ShippingCost.create from_country: nl, to_country: be, shipping_type: parcel, max_weight:  2000, cost:  9.00
ShippingCost.create from_country: nl, to_country: be, shipping_type: parcel, max_weight:  5000, cost: 20.00

# order from nl
ShippingCost.create from_country: nl, to_country: nl, shipping_type: letter, max_weight:   250, cost:  3.56
ShippingCost.create from_country: nl, to_country: nl, shipping_type: parcel, max_weight:  2000, cost:  6.75

## PRODUCT CATEGORIES

e_cig  = ProductCategory.create title: "E-sigaretten", position: 0, primary: false
agri   = e_cig.children.create title: "AGRI-serie", position: 0, primary: true
classy = e_cig.children.create title: "Classy", position: 1, primary: true

e_liq  = ProductCategory.create title: "E-liquids", position: 1, primary: true
e_liq.children.create title: "Tabak", position: 0, primary: false
e_liq.children.create title: "Fruit", position: 1, primary: false
e_liq.children.create title: "Overig", position: 2, primary: false

## PRODUCT ATTRIBUTES & VARIATIONS
color_clearomizer = Attribute.create title: "Kleur clearomizer",
  variations_attributes: [{title: "geel"}, {title: "groen"}, {title: "blauw"}, {title: "transparant"}, {title: "paars"}]
color_lanyard = Attribute.create title: "Kleur lanyard",
  variations_attributes: [{title: "geel"}, {title: "groen"}, {title: "blauw"}, {title: "zwart"}, {title: "paars"}]
color_battery = Attribute.create title: "Kleur batterij",
  variations_attributes: [{title: "zwart"}, {title: "zilver"}]
agri_type = Attribute.create title: "Type",
  variations_attributes: [{title: "AGRI-D"}, {title: "AGRI-USB"}, {title: "AGRI-V"}]
strength = Attribute.create title: "Sterkte",
  variations_attributes: [{title: "0mg"}, {title: "6mg"}, {title: "12mg"}, {title: "18mg"}]

## PRODUCTS

blister = Product.create title: "Blister", price: 0.0, category: agri, shipping_type: parcel, weight: 96, ships_from_belgium: true, primary: true, adult_only: true
blister.options << agri_type

giftbox = Product.create title: "Giftbox", price: 0.0, category: agri, shipping_type: parcel, weight: 248, ships_from_belgium: true, primary: true, adult_only: true
giftbox.options << agri_type

clearomizer = Product.create title: "Clearomizer", price: 0.0, category: agri, shipping_type: letter, weight: 11, ships_from_belgium: true, primary: false, adult_only: true
clearomizer.options << color_clearomizer

lanyard = Product.create title: "Lanyard", price: 0.0, category: agri, shipping_type: letter, weight: 11, ships_from_belgium: true, primary: false, adult_only: true
lanyard.options << color_lanyard

battery = Product.create title: "Batterij 650", price: 0.0, category: agri, shipping_type: letter, weight: 27, ships_from_belgium: true, primary: false, adult_only: true
battery.options << agri_type
battery.options << color_battery

Product.create title: "Tips", price: 0.0, category: agri, shipping_type: letter, weight: 3, ships_from_belgium: true, primary: false, adult_only: true
Product.create title: "Mouthpiece", price: 0.0, category: agri, shipping_type: letter, weight: 1, ships_from_belgium: true, primary: false, adult_only: false
Product.create title: "Cases", price: 0.0, category: agri, shipping_type: parcel, weight: 3, ships_from_belgium: true, primary: false, adult_only: true

Product.create title: "Classy medium (5pcs)", price: 0.0, category: classy, shipping_type: parcel, weight: 80, ships_from_belgium: true, primary: true, adult_only: false
Product.create title: "Classy large (20pcs)", price: 0.0, category: classy, shipping_type: parcel, weight: 260, ships_from_belgium: true, primary: true, adult_only: false