source 'https://rubygems.org'
# ruby '2.0.0'
# ruby-gemset=rails-4

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.3'

# Use sqlite3 as the development database for Active Record
group :development do
  gem 'sqlite3', '1.3.9'
end

group :production do
  # gem 'pg'
  gem 'mysql2'
  gem 'rails_12factor'
end

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'
gem 'compass-rails', '~> 1.1.7'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '2.4.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
gem 'coffee-script-source', '~> 1.7.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

gem 'activeadmin', github: 'activeadmin' # admin backend
gem "devise", "~> 3.2.3" # admin backend login
gem 'devise-i18n', '~> 0.11.2' # translations for devise
gem "friendly_id", "~> 5.0.3" # use seo-friendly urls for basic pages
gem "carrierwave", "~> 0.10.0" # file upload
gem "mini_magick", "~> 3.7.0" # image manipulation
gem "ckeditor", "~> 4.0.10" # wysiwyg editor used for body text
gem "acts-as-taggable-on", "~> 3.0.1" # tags
gem "acts_as_list", "~> 0.4.0" # ordered list of models
gem "ancestry", "~> 2.0.0" # hierarchical models
gem "activeadmin-sortable-tree", github: "nebirhos/activeadmin-sortable-tree", branch: "master" # sortable (hierarchical) lists in active admin
gem "whenever", "~> 0.9.2" # cron
gem "active_enum", github: "adzap/active_enum" # enum classes
gem "bootstrap-sass", "~> 3.1.1" # bootstrap
gem 'jquery-turbolinks', '~> 2.0.2' # bind turbolinks page load to document.ready
gem 'magnific-popup-rails', '~> 0.9.9' # magnific popup lightbox
gem 'acts_as_shopping_cart', '~> 0.2.0' # shopping cart
gem 'simple_form', '~> 3.1.0.rc1', github: 'plataformatec/simple_form' # simple form
gem 'fullcalendar-rails', '~> 1.6.4.0' # event calendar
gem 'momentjs-rails', '~> 2.5.0' # js date manipulation
gem 'bootstrap3-datetimepicker-rails', '~> 3.0.0' # date picker
gem 'cancan', '~> 1.6.10' # authorization
gem 'mail_form', '~> 1.5.0' # send mails from contact form
gem 'html_truncator', '~> 0.4.0' # generate teasers
gem 'rails-i18n', '~> 4.0.0'
gem 'just-datetime-picker', '~> 0.0.6'
# gem 'will_paginate', '~> 3.0.7' # pagination
gem 'kaminari', '~> 0.16.1' # pagination
gem 'valvat', '~> 0.6.8' # vat number validation

# generate pdf invoices
# gem 'wkhtmltopdf-binary'
gem 'wicked_pdf'

# capistrano
gem 'capistrano', '~> 3.2.1'
gem 'capistrano-bundler', '~> 1.1.2'
gem 'capistrano-rails', '~> 1.1.1'
gem 'capistrano-rbenv', github: "capistrano/rbenv"

gem 'i18n-workflow', github: 'moneybird/i18n-workflow'
